import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux';

class App extends Component {
 
render(){
  return (
    <div className="App">
     
        <button onClick={this.props.onAgeUp}>Add</button>
        {this.props.age}
        <button onClick={this.props.onAgeDown}>Subtract</button>
       
       
      
    </div>
  );

}
  
}

const mapStateToProps=(state)=>{
  return{
    age:state.age
  }
}
const mapDispachToProps=(dispach)=>{
  return{
    onAgeUp:()=>dispach({type:'Age_up'}),
    onAgeDown:()=>dispach({type:'Age_down'})

  }

}

export default connect(mapStateToProps,mapDispachToProps) (App);
